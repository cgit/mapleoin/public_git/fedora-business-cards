###
# fedora-business-cards - for rendering Fedora contributor business cards
# Copyright (C) 2008  Ian Weller <ianweller@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
###

"""
Command-line interface to business card generator. Takes no arguments; uses
optparser.OptionParser instead.
"""

from optparse import OptionParser
import os
import sys
from getpass import getpass

# local imports
from .. import config
from .. import information
from .. import generate
from .. import export


def cmdline_card_line(data):
    """
    Print a line of the business card for the cmdline frontend.
    """
    return "| %s%s |" % (data, ' '*(59-len(data)))


def main():
    """
    Call this to make things happen.
    """
    # setup option parser
    parser = OptionParser()
    parser.usage = "%prog [options]"
    parser.add_option("-d", "--dpi", dest="dpi", default=300, type="int",
                      help="DPI of exported file")
    parser.add_option("-t", "--template", dest="template",
                      default="northamerica", help="Name of template to use,"+\
                      " run with --list-templates to see a list")
    parser.add_option("--list-templates", action="store_true", default=False,
                      dest="listtemplates", help="List available templates")
    parser.add_option("-u", "--username", dest="username", default="",
                      help="If set, use a different name than the one logged"+\
                      " in with to fill out business card information")
    parser.add_option("-x", "--width", dest="width", default="",
                      help="If set, use a different height")
    parser.add_option("-y", "--height", dest="height", default="",
                      help="If set, use a different width")
    parser.add_option("--pdf", dest="output", default="png", const="pdf",
                      action="store_const", help="Export as PDF")
    parser.add_option("--png", dest="output", default="png", const="png",
                      action="store_const", help="Export as PNG (default)")
    parser.add_option("--svg", dest="output", default="png", const="svg",
                      action="store_const", help="Export as SVG")
    parser.add_option("--eps", dest="output", default="png", const="eps",
                      action="store_const", help="Export as EPS")
    parser.add_option("--cmyk-pdf", dest="output", default="png",
                      const="cmyk_pdf", action="store_const",
                      help="Export as PDF with CMYK color")
    parser.add_option("-c", "--config", dest="config_location", default="",
                      help="Location of config.ini configuration file")
    options = parser.parse_args()[0]
    # check what templates are available
    config.parser.read(options.config_location)
    templates = config.available_templates(config.parser)
    if options.listtemplates:
        print "Available templates:"
        for section in templates.sections():
            print "  %s (%s)" % (section, templates.get(section, 'humandesc'))
        sys.exit(0)
    if options.template not in templates.sections():
        print "%s not an available template" % options.template
        sys.exit(1)
    # ask for FAS login
    print "Login to FAS:"
    print "Username:",
    username = raw_input()
    password = getpass()
    if options.username == "":
        options.username = username
    infodict = information.get_information(username, password,
                                           options.username)
    # setup default content
    name = infodict['name']
    title = infodict['title']
    if infodict['gpgid'] == None:
        gpg = ''
    else:
        gpg = "GPG key ID: %s" % infodict['gpgid']
    if infodict['irc'] == None:
        lines = [infodict['email'],
                 infodict['phone'],
                 infodict['url'],
                 '',
                 gpg,
                 '']
    else:
        lines = [infodict['email'],
                 infodict['phone'],
                 infodict['irc']+" on irc.freenode.net",
                 infodict['url'],
                 '',
                 "GPG key ID: "+infodict['gpgid']]
    done_editing = False
    while not done_editing:
        print "Current business card layout:"
        print "   +"+"-"*61+"+"
        print " n "+cmdline_card_line(name)
        print " t "+cmdline_card_line(title)
        print "   "+cmdline_card_line('')
        for i in range(6):
            print (" %i " % i)+cmdline_card_line(lines[i])
        print "   "+cmdline_card_line('')
        print "   "+cmdline_card_line('')
        print "   "+cmdline_card_line('fedora'+' '*17+\
                                      'freedom | friends | features | first')
        print "   +"+"-"*61+"+"
        print "Enter a line number to edit, or [y] to accept:",
        lineno = raw_input()
        if lineno == "" or lineno == "y":
            done_editing = True
        else:
            print ("Enter new data for line %s:" % lineno),
            newdata = raw_input()
            if lineno == 'n':
                name = newdata
            elif lineno == 't':
                title = newdata
            elif lineno == '0' or lineno == '1' or lineno == '2' or \
                    lineno == '3' or lineno == '4' or lineno == '5':
                lines[int(lineno)] = newdata
    # figure out template locations
    frontloc = config.parser.get('location', 'templates')+'/'+\
            templates.get(options.template, 'front')
    backloc = config.parser.get('location', 'templates')+'/'+\
            templates.get(options.template, 'back')
    # generate front of business card
    print "Generating front...",
    sys.stdout.flush()
    xml = generate.gen_front(name, title, lines, frontloc, 
                             options.width, options.height)
    if options.output == "svg":
        export.svg_to_file(xml, options.username+'-front.'+options.output)
    elif options.output == "cmyk_pdf":
        export.svg_to_cmyk_pdf(xml, options.username+'-front.pdf')
    else:
        export.svg_to_pdf_png(xml, options.username+'-front.'+options.output,
                              options.output, options.dpi)
    # generate back of business card
    print "Generating back...",
    sys.stdout.flush()
    xml = generate.gen_back(backloc, options.width, options.height)
    if options.output == "svg":
        export.svg_to_file(xml, options.username+'-back.'+options.output)
    elif options.output == "cmyk_pdf":
        export.svg_to_cmyk_pdf(xml, options.username+'-back.pdf')
    else:
        export.svg_to_pdf_png(xml, options.username+'-back.'+options.output,
                              options.output, options.dpi)
    print "Done."
    sys.stdout.flush()
